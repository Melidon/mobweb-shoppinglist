package hu.bme.aut.android.shoppinglist

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import hu.bme.aut.android.shoppinglist.adapter.ShoppingAdapter
import hu.bme.aut.android.shoppinglist.data.ShoppingItem
import hu.bme.aut.android.shoppinglist.data.ShoppingListDatabase
import hu.bme.aut.android.shoppinglist.databinding.ActivityMainBinding
import hu.bme.aut.android.shoppinglist.fragments.NewShoppingItemDialogFragment
import kotlinx.android.synthetic.main.content_main.*
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity(), ShoppingAdapter.ShoppingItemClickListener,
    NewShoppingItemDialogFragment.NewShoppingItemDialogListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ShoppingAdapter
    private lateinit var database: ShoppingListDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        binding.fab.setOnClickListener {
            NewShoppingItemDialogFragment().show(
                supportFragmentManager,
                NewShoppingItemDialogFragment.TAG
            )
        }

        database = Room.databaseBuilder(
            applicationContext,
            ShoppingListDatabase::class.java,
            "shopping-list"
        ).build()

        initRecyclerView()
    }

    override fun onShoppingItemCreated(newItem: ShoppingItem) {
        thread {
            val newId = database.shoppingItemDao().insert(newItem)
            val newShoppingItem = newItem.copy(
                id = newId
            )
            runOnUiThread {
                adapter.addItem(newShoppingItem)
            }
        }
    }

    override fun onShoppingItemDelete(itemToDelete: ShoppingItem) {
        thread {
            database.shoppingItemDao().deleteItem(itemToDelete)
            runOnUiThread {
                adapter.deleteItem(itemToDelete)
            }
        }
    }

    override fun onItemEdit(itemToEdit: ShoppingItem) {
        NewShoppingItemDialogFragment(itemToEdit).show(
            supportFragmentManager,
            NewShoppingItemDialogFragment.TAG
        )
    }

    override fun onItemEdited(editedItem: ShoppingItem) {
        thread {
            database.shoppingItemDao().update(editedItem)
            runOnUiThread {
                adapter.editItem(editedItem)
            }
        }
    }

    fun removeAllShoppingItem() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Alert!")
        builder.setMessage("Are you sure you want to delete all items?")

        builder.setPositiveButton(R.string.ok) { _, _ ->
            thread {
                database.shoppingItemDao().deleteAll()
                runOnUiThread {
                    adapter.deleteAll()
                }
            }
        }

        builder.setNegativeButton(R.string.cancel) { _, _ -> }

        builder.show()
    }

    private fun initRecyclerView() {
        recyclerView = MainRecyclerView
        adapter = ShoppingAdapter(this)
        loadItemsInBackground()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    private fun loadItemsInBackground() {
        thread {
            val items = database.shoppingItemDao().getAll()
            runOnUiThread {
                adapter.update(items)
            }
        }
    }

    override fun onItemChanged(item: ShoppingItem) {
        thread {
            database.shoppingItemDao().update(item)
            Log.d("MainActivity", "ShoppingItem update was successful")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_remove_all -> {
                removeAllShoppingItem()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}