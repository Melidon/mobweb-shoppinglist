package hu.bme.aut.android.shoppinglist.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.shoppinglist.R
import hu.bme.aut.android.shoppinglist.data.ShoppingItem
import hu.bme.aut.android.shoppinglist.databinding.DialogNewShoppingItemBinding

class NewShoppingItemDialogFragment(private var item: ShoppingItem? = null) : DialogFragment() {

    interface NewShoppingItemDialogListener {
        fun onShoppingItemCreated(newItem: ShoppingItem)
        fun onItemEdited(item: ShoppingItem)
    }

    private lateinit var listener: NewShoppingItemDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? NewShoppingItemDialogListener
            ?: throw RuntimeException("Activity must implement the NewShoppingItemDialogListener interface!")
    }

    private lateinit var binding: DialogNewShoppingItemBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogNewShoppingItemBinding.inflate(
            LayoutInflater.from(
                context
            ), null, false
        )
        binding.ShoppingItemCategorySpinner.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.category_items)
        )

        item?.let {
            binding.ShoppingItemNameEditText.setText(it.name)
            binding.ShoppingItemDescriptionEditText.setText(it.description)
            binding.ShoppingItemEstimatedPriceEditText.setText(it.estimatedPrice.toString())
            binding.ShoppingItemCategorySpinner.setSelection(it.category.ordinal)
            binding.ShoppingItemIsPurchasedCheckBox.isChecked = it.isBought
        }

        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.new_shopping_item)
            .setView(binding.root)
            .setPositiveButton(R.string.ok) { _, _ ->
                if (isValid()) {
                    if (item == null) {
                        listener.onShoppingItemCreated(getShoppingItem())
                    } else {
                        listener.onItemEdited(getShoppingItem())
                    }
                }
            }
            .setNegativeButton(R.string.cancel, null)
            .create()
    }

    private fun isValid() = binding.ShoppingItemNameEditText.text.isNotEmpty()

    private fun getShoppingItem(): ShoppingItem {
        if (item == null) {
            return ShoppingItem(
                id = null,
                name = binding.ShoppingItemNameEditText.text.toString(),
                description = binding.ShoppingItemDescriptionEditText.text.toString(),
                estimatedPrice = try {
                    binding.ShoppingItemEstimatedPriceEditText.text.toString().toInt()
                } catch (e: java.lang.NumberFormatException) {
                    0
                },
                category = ShoppingItem.Category.getByOrdinal(binding.ShoppingItemCategorySpinner.selectedItemPosition)
                    ?: ShoppingItem.Category.BOOK,
                isBought = binding.ShoppingItemIsPurchasedCheckBox.isChecked
            )
        } else {
            // So ID will not change
            return item!!.copy(
                name = binding.ShoppingItemNameEditText.text.toString(),
                description = binding.ShoppingItemDescriptionEditText.text.toString(),
                estimatedPrice = try {
                    binding.ShoppingItemEstimatedPriceEditText.text.toString().toInt()
                } catch (e: java.lang.NumberFormatException) {
                    0
                },
                category = ShoppingItem.Category.getByOrdinal(binding.ShoppingItemCategorySpinner.selectedItemPosition)
                    ?: ShoppingItem.Category.BOOK,
                isBought = binding.ShoppingItemIsPurchasedCheckBox.isChecked
            )
        }
    }

    companion object {
        const val TAG = "NewShoppingItemDialogFragment"
    }
}
